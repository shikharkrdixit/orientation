# docker Summary

#### Content

- Introduction to docker
- Docker Usage and flow

### For Terminologies
 - [terms](#Terminologies "Goto Terminologies")

### Docker Installation
 - [install](#Installation "Goto Installation")
 
### Basic Command
 - [cmd](#Basic Commands: "Goto Basic Commands:")
 
### Docker Operations
 - [operation](#Operations on Docker: "Goto Operations on Docker:")
 
#### Purpose for Docker : 
It is hard to manage dependencies in large projects for deploying in different environments, so docker is used.

#### Terminologies
##### - Docker : 
_It is used to develop and run apps with containers._

##### - Docker image :
_It contains everything needed to run an app as a container. Includes : _
 -  code
 - runtime
 - libraries
 - environment variables
 - configuration files_
This image can be deployed to Docker environment as Container.



##### - Container :
_ It is a running Docker Image, from one image multiple containers can be created. _


##### - Docker Hub:
_  It is like GitHub but for docker images and containers. _


#### Installation
For Ubuntu 16.04.
- Uninstall the older version if already installed

```
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```
- Installing CE ( Community Docker Engine)

```
$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

// Check if docker is successfully installed in your system
$ sudo docker run hello-world
```
#### Basic Commands:

##### docker ps
Helps us to view all running containers in Docker Host.
example : 
```
$ docker ps

CONTAINER ID IMAGE  COMMAND CREATED        STATUS            PORTS NAMES
30986b73dc00 ubuntu "bash"  45 minutes ago Up About a minute                 elated_franklin
```
##### docker start
Starts any stopped containers.
example :
- To start container, ID 30986
```
$ docker start 30986
```
##### docker stop
Stops any running containers.
example:
- To stop container, ID 30986
```
$ docker stop 30986
```
##### docker run
Creates containers from docker images.
example:
```
$ docker run 30986
```
##### docker rm
Deletes the containers.
example:
```
$ docker rm 30986
```
### Operations on Docker:

Typically used flow :
- Download/pull docker image you want to work on.
- Copy your code in docker.
- Access docker terminal.
- Install and add required dependencies.
- Compile and Run the code.
- Document the steps in README.md file.
- Commit the changes.
- Push docker image to docker-hub.


example of the flow :

##### 1. Downloading :
```
docker pull snehabhapkar/trydock
```
##### 2. Run the docker image
```
docker run -ti snehabhapkar/trydock /bin/bash
```
Output :
```
root@e0b72ff850f8:/#
```
container ID: e0b72ff850f8.


##### 3. Copy the code inside docker with this command

Here a simple hello.py code is being used as
```
print("hey, I'm talking from container")
```
 - Copy file inside the docker container
 ```
 docker cp hello.py e0b72ff850f8:/
```

where e0b72ff850f8 is the containerID


- Write a script for installing dependencies - requirements.sh

```
apt update
apt install python3
```



- Copy file inside docker
```
docker cp requirements.sh e0b72ff850f8:/
```
##### 4. Install dependencies

- Give permission to run shell script
```
docker exec -it e0b72ff850f8 chmod +x requirements.sh
```

- Install dependencies
```
docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh
```

##### 5. Run the program in container with this command

```
docker start e0b72ff850f8
docker exec e0b72ff850f8 python3 hello.py
```
e0b72ff850f8 : containerID.


##### 6. Save the program in docker image with docker commit

```
docker commit e0b72ff850f8 snehabhapkar/trydock
```
e0b72ff850f8 : containerID.


##### 7. Push docker image to dockerhub

Tag image name with different name

```
docker tag snehabhapkar/trydock username/repo
```

username is username on dockerhub and repo is name of image.

example:
```
docker tag snehabhapkar/trydock yh42/trial-dock-img
```

- Push on dockerhub
```
docker push username/repo
```