# git Summary

##### Git is a version control software, helps in collaborative work.
##### It is used widely in industries.

### Vocabularies:
##### Repositories(repo):
It is collection of files and folders where the code is stored.

##### Gitlab:
Second most popular remote storage.

##### Commit:
Termed as saving your work, it exists on your local device until pushed on the repo.

##### Push:
Syncing the commits.

##### Branch :
Seperate instances of the code, these are different from the main code.

##### Merge:
Fusing two branches together when the branch is bug-free.

##### Clone:
Downloading entire repo over local device.

##### Fork:
Copying entire repo under one's name.

#### Getting started

##### Installation:

Operating System  | Way
------------- | -------------
Linux  | sudo apt-get install git :in terminal
Linux(If first doesnt work)  |   [refer this](https://git-scm.com/download/linux)
Windows | [refer this](https://git-scm.com/download/win)

##### Git has three main stages for file residing :
- Modified
    - file is changed but not yet committed.
- Staged
    - modified file is marked.
- Committed
   - file is finally stored in repo.


#### At an instance 3/4 different trees of your software:
1. Workspace : All the changes are made in this tree.
2. Staging : Staged files goes here.
3. Local Repository : Committed files go here.
4. Remote Repository : Copy of the local repo, changes should be pushed to the Remote Repo.

#### Git Workflow :

- Cloning:
  - $ git clone <link-to-repository> 
- New branch creation:
 - $ git checkout master
  $ git checkout -b <your-branch-name>
- Modify the files and select only those changes you want to commit
 - $ git add . (to add all files)
- Commit the changes to local repo permanently
 - $ git commit -sv (Comments)
- Push them to Remote repo
 - $ git push origin < branch-name >
 
- - - - -
 
 ### Credits :
 - B.J Keeton (git for beginners)
   - [source](https://www.elegantthemes.com/blog/resources/git-and-github-a-beginners-guide-for-complete-newbies)
 - git-scm for documentation
    - [source](https://git-scm.com/docs/)

